<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Beranda\BerandaController;
use App\Http\Controllers\Tentang\TentangController;
use App\Http\Controllers\Kontak\KontakController;

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\LogoutController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\Dashboard\DashboardController;
use App\Http\Controllers\Divisi\DivisiController;
use App\Http\Controllers\Unit\UnitController;
use App\Http\Controllers\Pengguna\PenggunaController;
use App\Http\Controllers\PegawaiSKP\PegawaiSKPController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect(route('beranda'));
});

// guest
Route::get('beranda', [BerandaController::class, 'index'])->name('beranda');
Route::get('beranda/fetch-api', [BerandaController::class, 'fetchApi'])->name('beranda-api');
Route::get('tentang-kami', [TentangController::class, 'index'])->name('tentang-kami');
Route::get('kontak', [KontakController::class, 'index'])->name('kontak');

// guest auth
Route::get('masuk', [LoginController::class, 'index'])->name('login');
Route::post('masuk-proses', [LoginController::class, 'loginProcess'])->name('login-process');
Route::get('daftar', [RegisterController::class, 'index'])->name('register');
Route::post('daftar-proses', [RegisterController::class, 'authStore'])->name('register-process');
Route::get('keluar', [LogoutController::class, 'logoutProcess'])->name('logout-process');

// auth

Route::name('auth.')->prefix('auth')->middleware('auth')->group(function() {

    Route::get('pegawai-skp', [PegawaiSKPController::class, 'index'])->name('pegawai-skp');
    Route::get('pegawai-skp/fetch-api', [PegawaiSKPController::class, 'fetchApi'])->name('pegawai-skp-api');
    Route::post('pegawai-skp/tambah', [PegawaiSKPController::class, 'employeeStore'])->name('pegawai-skp-tambah');
    Route::post('pegawai-skp/pilih', [PegawaiSKPController::class, 'employeeChoose'])->name('pegawai-skp-pilih');
    Route::post('pegawai-skp/hapus', [PegawaiSKPController::class, 'employeeDelete'])->name('pegawai-skp-hapus');

    Route::get('divisi', [DivisiController::class, 'index'])->name('divisi');
    Route::get('divisi/detail/{id}', [DivisiController::class, 'edit'])->name('divisi-detail');
    Route::post('divisi/detail/update', [DivisiController::class, 'update'])->name('divisi-update');
    Route::get('divisi/detail/{id}/delete', [DivisiController::class, 'delete'])->name('divisi-delete');
    Route::post('divisi/tambah', [DivisiController::class, 'divisionStore'])->name('divisi-tambah');

    Route::get('unit-kerja', [UnitController::class, 'index'])->name('unit-kerja');
    Route::get('unit-kerja/detail/{id}', [UnitController::class, 'edit'])->name('unit-kerja-detail');
    Route::post('unit-kerja/detail/update', [UnitController::class, 'update'])->name('unit-kerja-update');
    Route::get('unit-kerja/detail/{id}/delete', [UnitController::class, 'delete'])->name('unit-kerja-delete');
    Route::post('unit-kerja/tambah', [UnitController::class, 'unitStore'])->name('unit-kerja-tambah');

    Route::get('pengguna', [PenggunaController::class, 'index'])->name('pengguna');
    Route::get('pengguna/detail/{id}', [PenggunaController::class, 'edit'])->name('pengguna-detail');
    Route::post('pengguna/detail/update', [PenggunaController::class, 'update'])->name('pengguna-update');
    Route::get('pengguna/detail/{id}/delete', [PenggunaController::class, 'delete'])->name('pengguna-delete');
    Route::post('pengguna/tambah', [PenggunaController::class, 'userStore'])->name('pengguna-tambah');
});
