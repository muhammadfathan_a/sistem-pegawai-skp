var ipv4_address = $("#ipv4");
ipv4_address.inputmask({
    alias: "ip",
    greedy: false, //The initial mask shown will be "" instead of "-____".
});

var inputExpired = document.getElementById("lama_langganan");
var langganan = document.getElementById("expired");
var register = document.getElementById("register");

var register2 = new Date();
var register3 = register2.getFullYear() + "-" + (register2.getMonth() + 1) + "-" + register2.getDate();

document.getElementById("register").innerHTML = register3;

const inputHandler = function (e) {
    var date = new Date();

    // console.log("input ", inputExpired);
    var new_date = new Date(
        date.setMonth(date.getMonth() + parseInt(inputExpired.value))
    );

    var date_expired =
        new_date.getFullYear() +
        "-" +
        (new_date.getMonth() + 1) +
        "-" +
        new_date.getDate();

    if (inputExpired.value === "") 
    {
        langganan.innerHTML = "";
    } 
    else 
    {
        langganan.innerHTML = `Expire Pada <b>${date_expired}</b>.`;
    }
};

inputExpired.addEventListener("input", inputHandler);

function randomPassword(length) {
    var result = "";
    var characters =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    var charactersLength = characters.length;
    for (var i = 0; i < length; i++) {
        result += characters.charAt(
        Math.floor(Math.random() * charactersLength)
        );
    }
    return result;
}

function download(lama_langganan, user, ip) {
    var tanggal = new Date();

    var tanggal_register =
        tanggal.getFullYear() +
        "-" +
        (tanggal.getMonth() + 1) +
        "-" +
        tanggal.getDate();

    var expired = new Date(
        tanggal.setMonth(tanggal.getMonth() + parseInt(lama_langganan))
    );

    var tanggal_expired =
        expired.getFullYear() +
        "-" +
        (expired.getMonth() + 1) +
        "-" +
        expired.getDate();

    var password_radius = randomPassword(10);
    var pom = document.createElement("a");
    pom.setAttribute(
        "href",
        "data:text/plain;charset=utf-8," +
        encodeURIComponent(`
        :log warning "==================================" 
        :log warning "ANDESTA.MY.ID CONFIGURATION - START" 
        :log warning "==================================" 
        :log warning " " 

        # VPN CONNECTION 
        :log warning ">>> Setting Up VPN Connection ..."
        /interface ovpn-client add comment="<<<===|| OVPN – ANDESTA.MY.ID | STATUS: PAID | START: ${tanggal_register} |
        EXPIRED: ${tanggal_expired} ||===>>>" connect-to=andesta.my.id name=ovpn-chr-ANDESTA password="${password_radius}" port=1111 profile=default-encryption user="${user}"
        :log warning ">>> VPN Connection Setup Complete"

        # NAT RULE:log warning ">>> Setting Up NAT Rules ..."/ip firewall nat add disabled="no" action=dst-nat chain=dstnat comment="<<<===|| PORT FORWARD RULE || ANDESTA.MY.ID - 1 ||===>>>" dst-address="${ip}" dst-port="22" protocol=tcp to-ports=80/ip firewall nat add disabled="no" action=dst-nat chain=dstnat comment="<<<===|| PORT FORWARD RULE || ANDESTA.MY.ID - 2 ||===>>>" dst-address="${ip}" dst-port="80" protocol=tcp to-ports=80/ip firewall nat add action=masquerade chain=srcnat comment= "<<<===|| MASQUERADE PORT FORWARD RULE ||===>>>" out-interface=ovpn-chr-andesta:log warning ">>> NAT Rules Setup Complete":log warning " "

        # NETWATCH:log warning ">>> Setting Up Netwatch ..."/tool netwatch add comment="<<<===|| NETWATCH –ANDESTA.MY.ID ||===>>>" host=10.20.30.1:log warning ">>> Netwatch Setup Complete"

        # RADIUS:log warning ">>> Setting up RADIUS ..."
        /radius add comment="<<<===|| RADIUS – ANDESTA.MY.ID ||===>>>" address=10.20.30.1 secret="${password_radius}" service=ppp,hotspot
        :log warning ">>> RADIUS Setup Complete"

        :log warning " "
        :log warning "=================================="
        :log warning "ANDESTA.MY.ID CONFIGURATION - END"
        :log warning "=================================="
        `)
    );
    pom.setAttribute("download", `${user}.txt`);

    pom.style.display = "none";
    document.body.appendChild(pom);

    pom.click();

    document.body.removeChild(pom);
}

function addTextHTML() {
document.addtext.name.value = document.addtext.name.value + ".html";
}

function addTextTXT() {
document.addtext.name.value = document.addtext.name.value + ".txt";
}