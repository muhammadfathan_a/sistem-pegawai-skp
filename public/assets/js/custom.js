/**
 *
 * You can write your JS code here, DO NOT touch the default style file
 * because it will make it harder for you to update.
 *
 */

"use strict";
$(document).ready(function() {
    $('#auth-table').DataTable( {
        processing: true,
        bServerSide: false,
        ajax: {
            "url": "http://sistem-pegawai-skp.herokuapp.com/auth/pegawai-skp/fetch-api",
            // "url": "http://127.0.0.1:8000/auth/pegawai-skp/fetch-api",
            "dataSrc": ""
        },
        "scrollX": true,
        display: 'stripe',
        columns: [{
                data: 'name',
                name: 'name'
            },
            {
                data: 'nip',
                name: 'nip'
            },
            {
                data: 'division_name',
                name: 'division_name'
            },
            {
                data: 'unit_name',
                name: 'unit_name'
            },
            {
                data: 'score',
                name: 'score'
            },
            {
                data: 'designation',
                name: 'designation'
            },
            {
                data: 'information',
                name: 'information'
            },
        ]
    });
});

$(document).ready(function() {
    $('#beranda-table').DataTable( {
        processing: true,
        bServerSide: false,
        ajax: {
            "url": "http://sistem-pegawai-skp.herokuapp.com/beranda/fetch-api",
            // "url": "http://127.0.0.1:8000/beranda/fetch-api",
            "dataSrc": ""
        },
        "scrollX": true,
        display: 'stripe',
        columns: [{
                data: 'name',
                name: 'name'
            },
            {
                data: 'nip',
                name: 'nip'
            },
            {
                data: 'division_name',
                name: 'division_name'
            },
            {
                data: 'unit_name',
                name: 'unit_name'
            },
            {
                data: 'score',
                name: 'score'
            },
            {
                data: 'designation',
                name: 'designation'
            },
            {
                data: 'information',
                name: 'information'
            },
        ]
    });
});
