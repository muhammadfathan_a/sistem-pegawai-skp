<!-- Modal -->
<div class="modal fade" id="skpChoose" data-backdrop="static" data-keyboard="false" tabindex="-1"
aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">
                    Pilih Pegawai SKP
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="{{ route('auth.pegawai-skp-pilih') }}" class="needs-validation">
            @csrf
                <div class="modal-body pt-5">
                    <div class="row d-flex flex-wrap" style="justify-content: space-around;">

                        <div class="form-group col-sm-12 col-md-12">
                            <label for="employee">
                                Pilih Pegawai
                            </label>
                            <select class="form-control" name="employee" id="employee">
                                @foreach ($employees as $employee)
                                    <option value="{{ $employee->employee_id }}">
                                        {{
                                            $employee->title_ahead.' '.$employee->first_name.' '
                                            .$employee->last_name.', '.$employee->back_title
                                        }}
                                    </option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">
                                Masukkan gelar belakang
                            </div>
                        </div>

                        <div class="form-group col-sm-12 col-md-6">
                            <label for="score">
                                Nilai SKP
                            </label>
                            <input id="score" type="number" min="0" class="form-control" name="score" tabindex="1" autofocus>
                            <div class="invalid-feedback">
                                Masukkan nilai skp
                            </div>
                        </div>

                        <div class="form-group col-sm-12 col-md-6">
                            <label for="designation">
                                Sebutan
                            </label>
                            <input id="designation" type="text" class="form-control" name="designation" tabindex="1" autofocus>
                            <div class="invalid-feedback">
                                Masukkan sebutan
                            </div>
                        </div>

                        <div class="form-group col-sm-12 col-md-12">
                            <label for="information">
                                Keterangan
                            </label>
                            <textarea id="information" type="text" class="form-control" name="information"
                            tabindex="1" autofocus style="height: 115px;"></textarea>
                            <div class="invalid-feedback">
                                Masukkan keterangan
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">
                        Simpan
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
