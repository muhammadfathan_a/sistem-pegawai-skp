<!-- Modal -->
<div class="modal fade" id="unitAdd" data-backdrop="static" data-keyboard="false" tabindex="-1"
aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">
                    Tambah Unit Kerja
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('auth.unit-kerja-tambah') }}" method="POST">
            @csrf
            <div class="modal-body py-5">
                <div class="row" style="width: 100%;">
                    <div class="col-sm-12 col-md-12 mb-4">
                        <label for="unit_name">
                            Nama Unit Kerja
                        </label>
                        <input type="text" name="unit_name" id="unit_name" class="form-control" required>
                    </div>
                    <div class="col-sm-12 col-md-12">
                        <label for="unit_detail">
                            Keterangan Unit Kerja
                        </label>
                        <input type="text" name="unit_detail" id="unit_detail" class="form-control" required>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">
                    Simpan
                </button>
            </div>
            </form>
        </div>
    </div>
</div>
