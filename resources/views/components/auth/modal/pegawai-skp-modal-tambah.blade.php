<!-- Modal -->
<div class="modal fade" id="skpAdd" data-backdrop="static" data-keyboard="false" tabindex="-1"
aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">
                    Tambah Pegawai SKP
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="{{ route('auth.pegawai-skp-tambah') }}" class="needs-validation">
            @csrf
                <div class="modal-body pt-5" style="
                    max-height: 525px;
                    overflow-y: scroll;
                ">
                    <div class="row d-flex flex-wrap" style="justify-content: space-around;">

                        <div class="form-group col-sm-12 col-md-12">
                            <label for="nip">
                                NIP
                            </label>
                            <input id="nip" type="number" min="1" class="form-control" name="nip" tabindex="1" required>
                            <div class="invalid-feedback">
                                Masukkan Nomor Induk Pegawai
                            </div>
                        </div>

                        <div class="form-group col-sm-12 col-md-6">
                            <label for="first_name">
                                Nama Depan
                            </label>
                            <input id="first_name" type="text" class="form-control" name="first_name" tabindex="1" required>
                            <div class="invalid-feedback">
                                Masukkan Nama Depan
                            </div>
                        </div>

                        <div class="form-group col-sm-12 col-md-6">
                            <label for="last_name">
                                Nama Belakang
                            </label>
                            <input id="last_name" type="text" class="form-control" name="last_name" tabindex="1" required>
                            <div class="invalid-feedback">
                                Masukkan Nama Belakang
                            </div>
                        </div>

                        <div class="form-group col-sm-12 col-md-6">
                            <label for="title_ahead">
                                Gelar Depan
                            </label>
                            <input id="title_ahead" type="text" class="form-control" name="title_ahead" tabindex="1" autofocus>
                            <div class="invalid-feedback">
                                Masukkan gelar depan
                            </div>
                        </div>

                        <div class="form-group col-sm-12 col-md-6">
                            <label for="back_title">
                                Gelar Belakang
                            </label>
                            <input id="back_title" type="text" class="form-control" name="back_title" tabindex="1" autofocus>
                            <div class="invalid-feedback">
                                Masukkan gelar belakang
                            </div>
                        </div>

                        <div class="form-group col-sm-12 col-md-12">
                            <label for="unit">
                                Unit Kerja
                            </label>
                            <select class="form-control" name="unit" id="unit">
                                @foreach ($units as $unit)
                                    <option value="{{ $unit->unit_id }}">
                                        {{ $unit->unit_name }}
                                    </option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">
                                Masukkan gelar belakang
                            </div>
                        </div>

                        <div class="form-group col-sm-12 col-md-12">
                            <label for="division">
                                Divisi
                            </label>
                            <select class="form-control" name="division" id="division">
                                @foreach ($divisions as $division)
                                    <option value="{{ $division->division_id }}">
                                        {{ $division->division_name }} ({{ $division->division_detail }})
                                    </option>
                                @endforeach
                            </select>
                            <div class="invalid-feedback">
                                Masukkan gelar belakang
                            </div>
                        </div>

                        <div class="form-group col-sm-12 col-md-6">
                            <label for="score">
                                Nilai SKP
                            </label>
                            <input id="score" type="number" min="0" class="form-control" name="score" tabindex="1" autofocus>
                            <div class="invalid-feedback">
                                Masukkan nilai skp
                            </div>
                        </div>

                        <div class="form-group col-sm-12 col-md-6">
                            <label for="designation">
                                Sebutan
                            </label>
                            <input id="designation" type="text" class="form-control" name="designation" tabindex="1" autofocus>
                            <div class="invalid-feedback">
                                Masukkan sebutan
                            </div>
                        </div>

                        <div class="form-group col-sm-12 col-md-12">
                            <label for="information">
                                Keterangan
                            </label>
                            <textarea id="information" type="text" class="form-control" name="information"
                            tabindex="1" autofocus style="height: 115px;"></textarea>
                            <div class="invalid-feedback">
                                Masukkan keterangan
                            </div>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">
                        Simpan
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
