<!-- Modal -->
<div class="modal fade" id="skpDelete" data-backdrop="static" data-keyboard="false" tabindex="-1"
aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">
                    Pilih Pegawai SKP
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form method="POST" action="{{ route('auth.pegawai-skp-hapus') }}" class="needs-validation">
            @csrf
                <div class="modal-body pt-5">
                    <div class="row d-flex flex-wrap" style="justify-content: space-around;">

                        <div class="form-group col-sm-12 col-md-12">
                            <label for="employee">
                                Pilih Pegawai
                            </label>
                            <select class="form-control" name="employee" id="employee">
                                @foreach ($employees as $employee)
                                    @if ($employee->employee_id != Auth::user()->employee_id)
                                        <option value="{{ $employee->employee_id }}">
                                            {{
                                                $employee->title_ahead.' '.$employee->first_name.' '
                                                .$employee->last_name.', '.$employee->back_title
                                            }}
                                        </option>
                                    @endif
                                @endforeach
                            </select>
                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-danger">
                        Hapus
                    </button>
                </div>
            </form>
        </div>
    </div>
</div>
