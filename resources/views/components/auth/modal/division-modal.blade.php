<!-- Modal -->
<div class="modal fade" id="divisionAdd" data-backdrop="static" data-keyboard="false" tabindex="-1"
aria-labelledby="staticBackdropLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="staticBackdropLabel">
                    Tambah Divisi
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ route('auth.divisi-tambah') }}" method="POST">
            @csrf
            <div class="modal-body py-5">
                <div class="row" style="width: 100%;">
                    <div class="col-sm-12 col-md-12 mb-4">
                        <label for="division_name">
                            Nama Divisi
                        </label>
                        <input type="text" name="division_name" id="division_name" class="form-control" required>
                    </div>
                    <div class="col-sm-12 col-md-12">
                        <label for="division_detail">
                            Keterangan Divisi
                        </label>
                        <input type="text" name="division_detail" id="division_detail" class="form-control" required>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary">
                    Simpan
                </button>
            </div>
            </form>
        </div>
    </div>
</div>
