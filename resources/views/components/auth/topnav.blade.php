
<nav class="navbar navbar-expand-lg main-navbar">
    <a href="{{ route('beranda') }}" class="navbar-brand sidebar-gone-hide">
        KEMENDESA
    </a>
    <div class="navbar-nav">
        <a href="#" class="nav-link sidebar-gone-show" data-toggle="sidebar"><i class="fas fa-bars"></i></a>
    </div>
    <div class="nav-collapse">
        <a class="sidebar-gone-show nav-collapse-toggle nav-link" href="#">
            <i class="fas fa-ellipsis-v"></i>
        </a>
        <ul class="navbar-nav">
            <li class="nav-item {{ request()->is('beranda') ? 'active' : '' }}">
                <a href="{{ route('beranda') }}" class="nav-link">
                    Beranda
                </a>
            </li>
            <li class="nav-item {{ request()->is('tentang-kami') ? 'active' : '' }}">
                <a href="{{ route('tentang-kami') }}" class="nav-link">
                    Tetang Kami
                </a>
            </li>
            <li class="nav-item {{ request()->is('kontak') ? 'active' : '' }}">
                <a href="{{ route('kontak') }}" class="nav-link">
                    Kontak
                </a>
            </li>
        </ul>
    </div>
    <div class="form-inline ml-auto"></div>
    <ul class="navbar-nav navbar-right">
        <li class="dropdown">
            <a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
                <img alt="image" src="{{ asset('assets/img/avatar/avatar-4.png') }}" class="rounded-circle mr-1">
                <div class="d-sm-none d-lg-inline-block">
                    {{
                        Auth::user()->Employee->title_ahead.' '.Auth::user()->Employee->first_name.' '
                        .Auth::user()->Employee->last_name.', '.Auth::user()->Employee->back_title
                    }}
                </div>
            </a>
            <div class="dropdown-menu dropdown-menu-right">
                <a href="{{ route('logout-process') }}" class="dropdown-item has-icon text-danger">
                    <i class="fas fa-sign-out-alt"></i> Logout
                </a>
            </div>
        </li>
    </ul>
</nav>
