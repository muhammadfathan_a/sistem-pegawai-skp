<nav class="navbar navbar-secondary navbar-expand-lg">
    <div class="container">
        <ul class="navbar-nav">
            <li class="nav-item {{ request()->is('auth/pegawai-skp') ? 'active' : '' }}">
                <a href="{{ route('auth.pegawai-skp') }}" class="nav-link">
                    <i class="fas fa-users"></i>
                    <span> Pegawai SKP </span>
                </a>
            </li>
            @if (Auth::user()->Role->role_id == 1)
            <li class="nav-item {{ request()->is('auth/pengguna') ? 'active' : '' }}">
                <a href="{{ route('auth.pengguna') }}" class="nav-link">
                    <i class="fas fa-user-cog"></i>
                    <span> Pengguna </span>
                </a>
            </li>
            <li class="nav-item dropdown {{ request()->is('auth/divisi') ? 'active' : '' }}
            {{ request()->is('auth/unit-kerja') ? 'active' : '' }}">
                <a href="#" data-toggle="dropdown" class="nav-link has-dropdown">
                    <i class="far fa-clone"></i>
                    <span>
                        Master Data
                    </span>
                </a>
                <ul class="dropdown-menu">
                    <li class="nav-item">
                        <a href="{{ route('auth.divisi') }}" class="nav-link">
                            Divisi
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="{{ route('auth.unit-kerja') }}" class="nav-link">
                            Unit Kerja
                        </a>
                    </li>
                </ul>
            </li>
            @endif
        </ul>
    </div>
</nav>
