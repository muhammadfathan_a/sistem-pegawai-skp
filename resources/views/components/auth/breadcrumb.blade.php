<div class="section-header" style="margin-bottom: 105px;">
    <h1>
        {{ ucwords(str_replace('-', ' ', Request::segment(2))) }}
    </h1>
    <div class="section-header-breadcrumb">

        <div class="breadcrumb-item active">
            {{ ucwords(str_replace('-', ' ', Request::segment(2))) }}
        </div>

        @if (Request::segment(3))
            <div class="breadcrumb-item">
                {{ ucwords(str_replace('-', ' ', Request::segment(3))) }}
            </div>
        @endif

        @if (Request::segment(4))
            <div class="breadcrumb-item">
                {{ ucwords(str_replace('-', ' ', Request::segment(4))) }}
            </div>
        @endif
    </div>
</div>
