<footer class="main-footer">
    <div class="footer-left">
        Copyright &copy; {{ Date::now()->format('Y') }} <div class="bullet"></div>
        <a href="https://kemendesa.go.id/">
            KEMDENDESA
        </a>
    </div>
    {{-- <div class="footer-right"> 2.3.0 </div> --}}
</footer>
