@extends('layouts.app')

@section('content')
<div class="section-body">
    <h2 class="section-title">
        Hubungi Kami
    </h2>
    <p class="section-lead">
        <a href="https://www.facebook.com/kemendesa.1/" target="_blank" style="text-decoration: none !important;">
            <i style="font-size: 30px;" class="mx-3 mt-4 mb-3 fab fa-facebook"></i>
        </a>
        <a href="https://twitter.com/kemendesa?lang=id" target="_blank" style="text-decoration: none !important;">
            <i style="font-size: 30px;" class="mx-3 mt-4 mb-3 fab fa-twitter"></i>
        </a>
        <a href="https://www.instagram.com/kemendespdtt/?hl=en" target="_blank" style="text-decoration: none !important;">
            <i style="font-size: 30px;" class="mx-3 mt-4 mb-3 fab fa-instagram"></i>
        </a>
        <a href="https://youtube.com/kemendespdtt" target="_blank" style="text-decoration: none !important;">
            <i style="font-size: 30px;" class="mx-3 mt-4 mb-3 fab fa-youtube"></i>
        </a>
    </p>
    <div class="card">
        <div class="card-header">
            <h4> Alamat Kantor </h4>
        </div>
        <div class="card-body">
            <p>
                Jl. TMP Kalibata No.17, Jakarta Selatan,12750, DKI Jakarta, Indonesia Telp : 021 - 7994372
            </p>
            <p>
                Jl. Abdul Muis No.7, RT.2/RW.3, Gambir, Jakarta Pusat, DKI Jakarta,Indonesia Telp : 021 - 3500334
            </p>
        </div>
    </div>
</div>
@endsection
