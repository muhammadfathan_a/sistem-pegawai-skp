@extends('layouts.auth')

@section('content')
<div class="section-body">
    <h2 class="section-title">
        Unit Kerja Master Data Management
    </h2>
    <p class="section-lead">
        Halaman untuk manajemen data unit kerja yang berlaku di departemen KEMENDESA
    </p>
    <div class="card">
        <div class="card-header">
            <h4>
                Data Unit Kerja
            </h4>
        </div>
        <div class="card-body py-0">
            <div class="table-responsive">
                <table class="table table-striped table-md">
                    <tbody>
                        <tr>
                            <th class="text-center"> # </th>
                            <th> Nama Unit Kerja </th>
                            <th> Keterangan Unit Kerja </th>
                            <th class="text-center"> Opsi </th>
                        </tr>
                        @php $no = 1; @endphp
                        @foreach ($units as $unit)
                            <tr>
                                <td class="text-center">
                                    {{ $no++ }}
                                </td>
                                <td>
                                    {{ $unit->unit_name }}
                                </td>
                                <td>
                                    {{ $unit->unit_detail }}
                                </td>
                                <td align="center">
                                    <a href="{{ route('auth.unit-kerja-detail', ['id' => strtolower($unit->unit_id)]) }}"
                                    class="btn btn-secondary">
                                        Detail
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="pagination justify-content-center my-4">
                {{ $units->links('pagination::bootstrap-4') }}
            </div>
        </div>
        <div class="card-footer bg-whitesmoke">
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-primary shadow-sm" data-toggle="modal" data-target="#unitAdd">
                <i class="fas fa-plus"></i>
                Tambah Unit Kerja
            </button>
        </div>
    </div>
</div>
@endsection
