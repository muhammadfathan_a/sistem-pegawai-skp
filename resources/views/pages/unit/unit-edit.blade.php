@extends('layouts.auth')

@section('content')
<div class="section-body">
    <h2 class="section-title">
        Unit Kerja Master Data Management
    </h2>
    <p class="section-lead">
        Halaman untuk manajemen data unit kerja yang berlaku di departemen KEMENDESA
    </p>
    <div class="card">
        <div class="card-header">
            <h4>
                Data Unit Kerja "{{ strtoupper(str_replace('-', ' ', Request::segment(4))) }}"
            </h4>
        </div>
        <form action="{{ route('auth.unit-kerja-update') }}" method="POST">
            @csrf
            <input type="hidden" name="unit_id" value="{{ $units->first()->unit_id }}">
            <div class="card-body py-5">
                <div class="row" style="width: 100%;">
                    <div class="col-sm-12 col-md-12 mb-4">
                        <label for="unit_name">
                            Nama Unit Kerja
                        </label>
                        <input type="text" name="unit_name" id="unit_name" class="form-control"
                        value="{{ $units->first()->unit_name }}" required>
                    </div>
                    <div class="col-sm-12 col-md-12">
                        <label for="unit_detail">
                            Keterangan Unit Kerja
                        </label>
                        <input type="text" name="unit_detail" id="unit_detail" class="form-control"
                        value="{{ $units->first()->unit_detail }}" required>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="mr-1 btn btn-primary">
                    Perbarui
                </button>
                <a href="{{ route('auth.unit-kerja-delete', ['id' => strtolower(Request::segment(4))]) }}" class="ml-1 btn btn text-danger">
                    Hapus Unit Kerja "{{ strtoupper(Request::segment(4)) }}"
                </a>
            </div>
        </form>
    </div>
</div>
@endsection
