@extends('layouts.auth')

@section('content')
<div class="section-body">
    <h2 class="section-title">
        SKP Employees Score Data Management
    </h2>
    <p class="section-lead">
        Halaman untuk manajemen pegawai yang mengumpulkan nilai skp yang berlaku di departemen KEMENDESA.
    </p>
    <div class="card">
        <div class="card-header">
            <h4>
                Data Pegawai SKP
            </h4>
        </div>
        <div class="card-body">
            <table class="table table-striped datatables" id="auth-table" class="display" style="width:100%">
                <thead>
                    <tr>
                        <th> Nama </th>
                        <th> NIP </th>
                        <th> Jabatan </th>
                        <th> Unit Kerja </th>
                        <th> Nilai </th>
                        <th> Sebutan </th>
                        <th> Keterangan </th>
                    </tr>
                </thead>
                {{-- <tfoot>
                    <tr>
                        <th> Nama </th>
                        <th> NIP </th>
                        <th> Jabatan </th>
                        <th> Unit Kerja </th>
                        <th> Nilai </th>
                        <th> Sebutan </th>
                        <th> Keterangan </th>
                    </tr>
                </tfoot> --}}
            </table>
        </div>
        <div class="card-footer bg-whitesmoke">
            <!-- Button trigger modal -->
            <button type="button" class="mr-1 btn btn-primary shadow-sm" data-toggle="modal" data-target="#skpAdd">
                <i class="fas fa-plus"></i>
                Tambah Pegawai SKP
            </button>
            <button type="button" class="ml-1 btn btn-primary shadow-sm" data-toggle="modal" data-target="#skpChoose">
                <i class="fas fa-check"></i>
                Pilih Pegawai SKP
            </button>
            <button type="button" class="ml-1 btn btn-danger shadow-sm" data-toggle="modal" data-target="#skpDelete">
                <i class="fas fa-trash"></i>
                Hapus Pegawai SKP
            </button>
        </div>
    </div>
</div>
@endsection
