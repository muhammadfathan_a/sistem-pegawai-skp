@extends('layouts.app')

@section('content')
<div class="section-body">
    <div class="card">
        <div class="card-header">
            <h4>
                Data Pegawai SKP
            </h4>
        </div>
        <div class="card-body">
            <table class="table table-striped datatables" id="beranda-table" class="display" style="width:100%">
                <thead>
                    <tr>
                        <th> Nama </th>
                        <th> NIP </th>
                        <th> Jabatan </th>
                        <th> Unit Kerja </th>
                        <th> Nilai </th>
                        <th> Sebutan </th>
                        <th> Keterangan </th>
                    </tr>
                </thead>
                {{-- <tfoot>
                    <tr>
                        <th> Nama </th>
                        <th> NIP </th>
                        <th> Jabatan </th>
                        <th> Unit Kerja </th>
                        <th> Nilai </th>
                        <th> Sebutan </th>
                        <th> Keterangan </th>
                    </tr>
                </tfoot> --}}
            </table>
        </div>
    </div>
</div>
@endsection
