@extends('layouts.app')

@section('content')
<div class="section-body">
    <h2 class="section-title">
        Tentang Kami
    </h2>
    <p class="section-lead">
        Kementerian Desa, Pembangunan Daerah Tertinggal dan Transmigrasi RI.
    </p>
    <div class="card">
        <div class="card-header">
            <h4> TENTANG KEMENDESA </h4>
        </div>
        <div class="card-body">
            <p>
                Kementerian Desa, PDT dan Transmigrasi adalah kementerian dalam Pemerintah Indonesia yang
                membidangi urusan pembangunan desa dan kawasan perdesaan, pemberdayaan masyarakat desa,
                percepatan pembangunan daerah tertinggal, dan transmigrasi. Kementerian Desa, Pembangunan
                Daerah Tertinggal, dan Transmigrasi berada di bawah dan bertanggung jawab kepada Presiden.
                <a href="http://kemendesa.go.id/berita" target="_blank"> berita dan pengumuman </a>.
            </p>
        </div>
    </div>
</div>
@endsection
