@extends('layouts.app')

@section('content')
<section class="section">
    <div class="container mt-5">
        <div class="row">
            <div class="col-12 col-sm-8 offset-sm-2 col-md-6 col-lg-6 col-xl-8">

                <div class="login-brand">
                    <img src="{{ asset('assets/kemendes/kemendes.png') }}" alt="logo" width="150" class="p-2 shadow-light rounded-circle">
                    <h5 class="mt-4">
                        Kementrian Desa, Pembangunan Daerah Tertinggal dan Transmigrasi
                    </h5>
                </div>

                <div class="card card-primary">
                    <div class="card-header"><h4>Login</h4></div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('login-process') }}" class="needs-validation">
                        @csrf
                            <div class="d-flex flex-wrap" style="justify-content: space-around;">
                                <div class="mx-2 form-group" style="width: 45%;">
                                    <div class="d-block">
                                        <label for="username" class="control-label">
                                            Username
                                        </label>
                                    </div>
                                    <input id="username" type="username" class="form-control" name="username" tabindex="2" required>
                                    <div class="invalid-feedback">
                                        Masukkan Username Anda
                                    </div>
                                </div>

                                <div class="mx-2 form-group" style="width: 45%;">
                                    <div class="d-block">
                                        <label for="password" class="control-label">
                                            Password
                                        </label>
                                    </div>
                                    <input id="password" type="password" class="form-control" name="password" tabindex="2" required>
                                    <div class="invalid-feedback">
                                        Masukkan Password Anda
                                    </div>
                                </div>
                                <div class="mx-2 mt-4 form-group" style="width: 45%;">
                                    <button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="4">
                                        Login
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="simple-footer">
                    Copyright &copy; <a href="https://www.kemendesa.go.id/" target="_blank"> KEMENDESA </a> {{ Date::now()->format('Y') }}
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
