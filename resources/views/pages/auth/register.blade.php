@extends('layouts.app')

@section('content')
<section class="section">
    <div class="container">
        <div class="row">
            <div class="col-12 col-sm-8 offset-sm-2 col-md-6 col-lg-6 col-xl-8">

                <div class="login-brand">
                    <img src="{{ asset('assets/kemendes/kemendes.png') }}" alt="logo" width="150" class="p-2 shadow-light rounded-circle">
                    <h5 class="mt-4">
                        Kementrian Desa, Pembangunan Daerah Tertinggal dan Transmigrasi
                    </h5>
                </div>

                <div class="card card-primary">

                    <div class="card-header">
                        <h4>
                            Daftar
                        </h4>
                    </div>

                    <div class="card-body">
                        <livewire:auth.register />
                    </div>
                </div>
                <div class="simple-footer">
                    Copyright &copy; KEMENDESA. {{ Date::now()->format('Y') }}
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
