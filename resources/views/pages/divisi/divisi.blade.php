@extends('layouts.auth')

@section('content')
<div class="section-body">
    <h2 class="section-title">
        Divisi Master Data Management
    </h2>
    <p class="section-lead">
        Halaman untuk manajemen data divisi yang berlaku di departemen KEMENDESA
    </p>
    <div class="card">
        <div class="card-header">
            <h4>
                Data Divisi
            </h4>
        </div>
        <div class="card-body py-0">
            <div class="table-responsive">
                <table class="table table-striped table-md">
                    <tbody>
                        <tr>
                            <th class="text-center"> # </th>
                            <th> Nama Divisi </th>
                            <th> Keterangan Divisi </th>
                            <th class="text-center"> Opsi </th>
                        </tr>
                        @php $no = 1; @endphp
                        @foreach ($divisions as $division)
                            <tr>
                                <td class="text-center">
                                    {{ $no++ }}
                                </td>
                                <td>
                                    {{ $division->division_name }}
                                </td>
                                <td>
                                    {{ $division->division_detail }}
                                </td>
                                <td class="text-center">
                                    <a href="{{ route('auth.divisi-detail', ['id' => strtolower($division->division_id)]) }}"
                                    class="btn btn-secondary">
                                        Detail
                                    </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <div class="pagination justify-content-center my-4">
                {{ $divisions->links('pagination::bootstrap-4') }}
            </div>
        </div>
        <div class="card-footer bg-whitesmoke">
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-primary shadow-sm" data-toggle="modal" data-target="#divisionAdd">
                <i class="fas fa-plus"></i>
                Tambah Divisi
            </button>
        </div>
    </div>
</div>
@endsection
