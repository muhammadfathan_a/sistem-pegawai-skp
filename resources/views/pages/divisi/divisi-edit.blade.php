@extends('layouts.auth')

@section('content')
<div class="section-body">
    <h2 class="section-title">
        Divisi Master Data Management
    </h2>
    <p class="section-lead">
        Halaman untuk manajemen data divisi yang berlaku di departemen KEMENDESA
    </p>
    <div class="card">
        <div class="card-header">
            <h4>
                Data Divisi "{{ strtoupper(str_replace('-', ' ', Request::segment(4))) }}"
            </h4>
        </div>
        <form action="{{ route('auth.divisi-update') }}" method="POST">
            @csrf
            <input type="hidden" name="division_id" value="{{ $divisions->first()->division_id }}">
            <div class="card-body py-5">
                <div class="row" style="width: 100%;">
                    <div class="col-sm-12 col-md-12 mb-4">
                        <label for="division_name">
                            Nama Divisi
                        </label>
                        <input type="text" name="division_name" id="division_name" class="form-control"
                        value="{{ $divisions->first()->division_name }}" required>
                    </div>
                    <div class="col-sm-12 col-md-12">
                        <label for="division_detail">
                            Keterangan Divisi
                        </label>
                        <input type="text" name="division_detail" id="division_detail" class="form-control"
                        value="{{ $divisions->first()->division_detail }}" required>
                    </div>
                </div>
            </div>
            <div class="card-footer">
                <button type="submit" class="mr-1 btn btn-primary">
                    Perbarui
                </button>
                <a href="{{ route('auth.divisi-delete', ['id' => strtolower(Request::segment(4))]) }}" class="ml-1 btn btn text-danger">
                    Hapus Divisi "{{ strtoupper(Request::segment(4)) }}"
                </a>
            </div>
        </form>
    </div>
</div>
@endsection
