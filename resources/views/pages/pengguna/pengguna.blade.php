@extends('layouts.auth')

@section('content')
<div class="section-body">
    <h2 class="section-title">
        Users Data Management
    </h2>
    <p class="section-lead">
        Halaman untuk manajemen pengguna superadmin/admin yang berlaku di departemen KEMENDESA.
    </p>
    <div class="card">
        <div class="card-header">
            <h4>
                Data Pengguna
            </h4>
        </div>
        <div class="card-body">
            <div class="row mx-auto" style="width: 100%;">
                @foreach ($employees as $employee)
                    <div class="col-sm-12 col-md-4">
                        <div class="card shadow mb-0">
                            <div class="card-header">
                                <h6 class="my-2" style="font-weight: 900; line-height: 25px;">
                                    <small>
                                        <small>
                                            {{ $employee->nip }}
                                        </small>
                                    </small>
                                    <br>
                                    {{
                                        $employee->title_ahead.' '.$employee->first_name.' '
                                        .$employee->last_name.', '.$employee->back_title
                                    }}
                                    <br>
                                    <small class="badge badge-primary">
                                        {{ $employee->role_name }}
                                    </small>
                                </h6>
                            </div>
                            <div class="card-body">
                                <span style="font-size: 13px;">
                                    {{ $employee->unit_name }} <br>
                                    <small>
                                        {{ $employee->division_detail }}
                                    </small>
                                </span>
                            </div>
                            <div class="card-footer pt-0">
                                <a href="{{ route('auth.pengguna-detail', ['id' => strtolower($employee->nip)]) }}"
                                class="btn-sm btn-secondary" style="text-decoration: none;">
                                    Detail
                                </a>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="pagination justify-content-center my-4">
                {{ $employees->links('pagination::bootstrap-4') }}
            </div>
        </div>
        <div class="card-footer bg-whitesmoke">
            <!-- Button trigger modal -->
            <button type="button" class="btn btn-primary shadow-sm" data-toggle="modal" data-target="#userAdd">
                <i class="fas fa-plus"></i>
                Tambah Pengguna
            </button>
        </div>
    </div>
</div>
@endsection
