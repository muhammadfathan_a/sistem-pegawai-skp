<div>
    <form method="POST" action="{{ route('register-process') }}" class="needs-validation">
    @csrf
        <div class="row d-flex flex-wrap" style="justify-content: space-around;">

            <div class="form-group col-sm-12 col-md-12">
                <label for="nip">
                    NIP
                </label>
                <input id="nip" type="number" min="1" class="form-control" name="nip" tabindex="1" required>
                <div class="invalid-feedback">
                    Masukkan Nomor Induk Pegawai
                </div>
            </div>

            <div class="form-group col-sm-12 col-md-6">
                <label for="first_name">
                    Nama Depan
                </label>
                <input id="first_name" type="text" class="form-control" name="first_name" tabindex="1" required>
                <div class="invalid-feedback">
                    Masukkan Nama Depan
                </div>
            </div>

            <div class="form-group col-sm-12 col-md-6">
                <label for="last_name">
                    Nama Belakang
                </label>
                <input id="last_name" type="text" class="form-control" name="last_name" tabindex="1" required>
                <div class="invalid-feedback">
                    Masukkan Nama Belakang
                </div>
            </div>

            <div class="form-group col-sm-12 col-md-6">
                <label for="title_ahead">
                    Gelar Depan
                </label>
                <input id="title_ahead" type="text" class="form-control" name="title_ahead" tabindex="1" autofocus>
                <div class="invalid-feedback">
                    Masukkan gelar depan
                </div>
            </div>

            <div class="form-group col-sm-12 col-md-6">
                <label for="back_title">
                    Gelar Belakang
                </label>
                <input id="back_title" type="text" class="form-control" name="back_title" tabindex="1" autofocus>
                <div class="invalid-feedback">
                    Masukkan gelar belakang
                </div>
            </div>

            <div class="form-group col-sm-12 col-md-12">
                <label for="unit">
                    Unit Kerja
                </label>
                <select class="form-control" name="unit" id="unit">
                    @foreach ($units as $unit)
                        <option value="{{ $unit->unit_id }}">
                            {{ $unit->unit_name }}
                        </option>
                    @endforeach
                </select>
                <div class="invalid-feedback">
                    Masukkan gelar belakang
                </div>
            </div>

            <div class="form-group col-sm-12 col-md-12">
                <label for="division">
                    Divisi
                </label>
                <select class="form-control" name="division" id="division">
                    @foreach ($divisions as $division)
                        <option value="{{ $division->division_id }}">
                            {{ $division->division_name }} ({{ $division->division_detail }})
                        </option>
                    @endforeach
                </select>
                <div class="invalid-feedback">
                    Masukkan gelar belakang
                </div>
            </div>

            <div class="form-group col-sm-12 col-md-12">
                <label for="role">
                    Role / Hak Akses
                </label>
                <select class="form-control" name="role" id="role">
                    @foreach ($roles as $role)
                        <option value="{{ $role->role_id }}">
                            {{ $role->role_name }}
                        </option>
                    @endforeach
                </select>
                <div class="invalid-feedback">
                    Masukkan gelar belakang
                </div>
            </div>

        </div>
        <hr>
        <div class="d-flex flex-wrap" style="justify-content: space-around;">
            <div class="form-group col-sm-12 col-md-6">
                <div class="d-block">
                    <label for="username" class="control-label">
                        Username
                    </label>
                </div>
                <input id="username" type="username" class="form-control" name="username" tabindex="2" required>
                <div class="invalid-feedback">
                    Masukkan Username Anda
                </div>
            </div>

            <div class="form-group col-sm-12 col-md-6">
                <div class="d-block">
                    <label for="password" class="control-label">Password</label>
                </div>
                <input id="password" type="password" class="form-control" name="password" tabindex="2" required>
                <div class="invalid-feedback">
                    please fill in your password
                </div>
            </div>
        </div>
        <div class="mx-auto mt-4 form-group" style="width: 45%;">
            <button type="submit" class="btn btn-primary btn-lg btn-block" tabindex="4">
                Daftar
            </button>
        </div>
    </form>
</div>
