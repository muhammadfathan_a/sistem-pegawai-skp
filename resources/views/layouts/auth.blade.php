<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title>{{ ucwords(str_replace('-', ' ', Request::segment(2))) }} &mdash; KEMENDESA </title>
    <link rel="shortcut icon" type="image/png" href="{{ asset('assets/kemendes/kemendes.png') }}"/>

    <!-- General CSS Files -->
    <link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/fontawesome/css/all.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/datatables/datatables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendor/pace/pace-theme-minimal.css') }}">

    <!-- CSS Libraries -->

    <!-- Template CSS -->
    <link rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/css/components.css') }}">
    @livewireStyles
</head>

<body class="layout-3" style="zoom: 100%;">

    <div id="app">
        <div class="main-wrapper container">
        <div class="navbar-bg"></div>

        @include('components.auth.topnav')
        @include('components.auth.botnav')

        <!-- Main Content -->
        <div class="main-content">
            <section class="section">

                @include('components.auth.breadcrumb')

                @yield('content')

            </section>
        </div>

        @include('components.auth.footer')
    </div>

    @if (Request::segment(2) == 'divisi')
        @include('components.auth.modal.division-modal')
    @endif

    @if (Request::segment(2) == 'unit-kerja')
        @include('components.auth.modal.unit-modal')
    @endif

    @if (Request::segment(2) == 'pengguna')
        @include('components.auth.modal.user-modal')
    @endif

    @if (Request::segment(2) == 'pegawai-skp')
        @include('components.auth.modal.pegawai-skp-modal-tambah')
        @include('components.auth.modal.pegawai-skp-modal-pilih')
        @include('components.auth.modal.pegawai-skp-modal-hapus')
    @endif

    <!-- General JS Scripts -->
    <script src="{{ asset('assets/templates/jquery-3.3.1.min.js') }}"></script>
    <script src="{{ asset('assets/templates/popper.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/datatables/datatables.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/pace/pace.js') }}"></script>
    <script src="{{ asset('assets/templates/jquery.nicescroll.min.js') }}"></script>
    <script src="{{ asset('assets/templates/moment.min.js') }}"></script>
    <script src="{{ asset('assets/js/stisla.js') }}"></script>

    <!-- JS Libraies -->

    <!-- Page Specific JS File -->

    <!-- Template JS File -->
    <script src="{{ asset('assets/js/scripts.js') }}"></script>
    <script src="{{ asset('assets/js/custom.js') }}"></script>
    @include('sweetalert::alert')
    @livewireScripts
</body>
</html>
