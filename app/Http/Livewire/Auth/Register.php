<?php

namespace App\Http\Livewire\Auth;

use Livewire\Component;
use DB;

class Register extends Component
{
    public function render()
    {
        $units = DB::table('units')->get();
        $divisions = DB::table('divisions')->get();
        $roles = DB::table('roles')->get();

        return view('livewire.auth.register', [
            'units' =>  $units,
            'divisions' =>  $divisions,
            'roles' =>  $roles,
        ]);
    }
}
