<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Alert;
use Hash;
use Date;
use DB;

class RegisterController extends Controller
{

    public $nip, $firstName, $lastName, $titleAhead, $backTitle, $division, $username, $password, $role;

    public function authStore(Request $request)
    {
        $this->nip = $request->nip;
        $this->firstName = $request->first_name;
        $this->lastName = $request->last_name;
        $this->titleAhead = $request->title_ahead;
        $this->backTitle = $request->back_title;
        $this->unit = $request->unit;
        $this->division = $request->division;

        $this->username = $request->username;
        $this->password = $request->password;
        $this->role = $request->role;

        DB::beginTransaction();
        try {
            DB::table('employees')->insert([
                'nip'           =>  $this->nip,
                'first_name'    =>  $this->firstName,
                'last_name'     =>  $this->lastName,
                'title_ahead'   =>  $this->titleAhead,
                'back_title'    =>  $this->backTitle,
                'division_id'   =>  $this->division,
                'unit_id'       =>  $this->unit,
            ]);

            $employeeId =   DB::table('employees')->select('employee_id')
                            ->where('nip', $this->nip)
                            ->first();

            DB::table('employees_login')->insert([
                'employee_id'   =>  $employeeId->employee_id,
                'username'      =>  $this->username,
                'password'      =>  Hash::make($this->password),
                'role_id'       =>  $this->role,
            ]);

            DB::commit();

            return redirect(route('login'))->withToastSuccess('Berhasil mendaftar, silakan login');;
        }
        catch (\Exception $e)
        {
            DB::rollback();
            return redirect(route('register'))->withToastDanger('Gagal mendaftar, coba kembali');;
        }
    }

    public function index()
    {
        if (Auth::user()) {
            return redirect(route('auth.dashboard'));
        }
        else {
            return view('pages.auth.register');
        }
    }
}
