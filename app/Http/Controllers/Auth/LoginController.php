<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    public function index()
    {
        if (Auth::user()) {
            return redirect(route('auth.dashboard'));
        }
        else {
            return view('pages.auth.login');
        }
    }

    public function loginProcess(Request $request)
    {
        $credentials = $request->only('username', 'password');

        if (Auth::attempt($credentials)) {
            $request->session()->regenerate();

            return redirect()->intended('auth/pegawai-skp');
        }

        return back()->withErrors([
            'username' => 'username tidak ditemukan, periksa kembali username anda',
        ]);
    }
}
