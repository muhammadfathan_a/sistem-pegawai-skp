<?php

namespace App\Http\Controllers\Pengguna;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Alert;
use Hash;
use Date;
use DB;

class PenggunaController extends Controller
{
    public function index()
    {
        $employees  =   DB::table('employees AS em')
                        ->join('units AS un', 'em.unit_id', '=', 'un.unit_id')
                        ->join('divisions AS dv', 'em.division_id', '=', 'dv.division_id')
                        ->join('employees_login AS el', 'em.employee_id', '=', 'el.employee_id')
                        ->join('roles AS ro', 'el.role_id', '=', 'ro.role_id')
                        ->paginate(9);

        $units = DB::table('units')->get();
        $divisions = DB::table('divisions')->get();
        $roles = DB::table('roles')->get();

        return view('pages.pengguna.pengguna', [
            'employees' =>  $employees,
            'units' =>  $units,
            'divisions' =>  $divisions,
            'roles' =>  $roles,
        ]);
    }

    public $nip, $firstName, $lastName, $titleAhead, $backTitle, $division, $username, $password, $role;
    public function userStore(Request $request)
    {
        $this->nip = $request->nip;
        $this->firstName = $request->first_name;
        $this->lastName = $request->last_name;
        $this->titleAhead = $request->title_ahead;
        $this->backTitle = $request->back_title;
        $this->unit = $request->unit;
        $this->division = $request->division;

        $this->username = $request->username;
        $this->password = $request->password;
        $this->role = $request->role;

        DB::beginTransaction();
        try {
            DB::table('employees')->insert([
                'nip'           =>  $this->nip,
                'first_name'    =>  $this->firstName,
                'last_name'     =>  $this->lastName,
                'title_ahead'   =>  $this->titleAhead,
                'back_title'    =>  $this->backTitle,
                'division_id'   =>  $this->division,
                'unit_id'       =>  $this->unit,
            ]);

            $employeeId =   DB::table('employees')->select('employee_id')
                            ->where('nip', $this->nip)
                            ->first();

            DB::table('employees_login')->insert([
                'employee_id'   =>  $employeeId->employee_id,
                'username'      =>  $this->username,
                'password'      =>  Hash::make($this->password),
                'role_id'       =>  $this->role,
            ]);

            DB::commit();

            return redirect(route('auth.pengguna'))->withToastSuccess('Berhasil menambahkan penggguna!');;
        }
        catch (\Exception $e)
        {
            DB::rollback();
            return redirect(route('auth.pengguna'))->withToastDanger('Gagal menambahkan penggguna');
        }
    }

    public function edit($id)
    {
        $employees  =   DB::table('employees AS em')
                        ->where('em.nip', $id)
                        ->join('units AS un', 'em.unit_id', '=', 'un.unit_id')
                        ->join('divisions AS dv', 'em.division_id', '=', 'dv.division_id')
                        ->join('employees_login AS el', 'em.employee_id', '=', 'el.employee_id')
                        ->join('roles AS ro', 'el.role_id', '=', 'ro.role_id')
                        ->get();

        $units = DB::table('units')->get();
        $divisions = DB::table('divisions')->get();
        $roles = DB::table('roles')->get();

        return view('pages.pengguna.pengguna-edit', [
            'employees' =>  $employees,
            'units' =>  $units,
            'divisions' =>  $divisions,
            'roles' =>  $roles,
        ]);
    }

    public function update(Request $request)
    {
        $this->nip = $request->nip;
        $this->firstName = $request->first_name;
        $this->lastName = $request->last_name;
        $this->titleAhead = $request->title_ahead;
        $this->backTitle = $request->back_title;
        $this->unit = $request->unit;
        $this->division = $request->division;

        $this->username = $request->username;
        $this->password = $request->password;
        $this->role = $request->role;

        DB::beginTransaction();
        try {
            if ($request->password == NULL)
            {
                DB::table('employees')
                ->where('employee_id', $request->employee_id)
                ->update([
                    'nip'           =>  $this->nip,
                    'first_name'    =>  $this->firstName,
                    'last_name'     =>  $this->lastName,
                    'title_ahead'   =>  $this->titleAhead,
                    'back_title'    =>  $this->backTitle,
                    'division_id'   =>  $this->division,
                    'unit_id'       =>  $this->unit,
                ]);

                $employeeId =   DB::table('employees')->select('employee_id')
                                ->where('nip', $this->nip)
                                ->first();

                DB::table('employees_login')
                ->where('employee_id', $request->employee_id)
                ->update([
                    'employee_id'   =>  $employeeId->employee_id,
                    'username'      =>  $this->username,
                    'role_id'       =>  $this->role,
                ]);
            }
            else
            {
                DB::table('employees')
                ->where('employee_id', $request->employee_id)
                ->update([
                    'nip'           =>  $this->nip,
                    'first_name'    =>  $this->firstName,
                    'last_name'     =>  $this->lastName,
                    'title_ahead'   =>  $this->titleAhead,
                    'back_title'    =>  $this->backTitle,
                    'division_id'   =>  $this->division,
                    'unit_id'       =>  $this->unit,
                ]);

                $employeeId =   DB::table('employees')->select('employee_id')
                                ->where('nip', $this->nip)
                                ->first();

                DB::table('employees_login')
                ->where('employee_id', $request->employee_id)
                ->update([
                    'employee_id'   =>  $employeeId->employee_id,
                    'username'      =>  $this->username,
                    'password'      =>  Hash::make($this->password),
                    'role_id'       =>  $this->role,
                ]);
            }

            DB::commit();
            return redirect(route('auth.pengguna'))->withToastSuccess('Pengguna berhasil diperbarui!');

        }
        catch (\Throwable $th)
        {
            DB::rollback();
            return redirect(route('auth.pengguna'))->withToastDanger('Unit kerja gagal diperbarui');
        }
    }

    public function delete($id)
    {
        DB::beginTransaction();
        try {

            $employeeId =   DB::table('employees')
                            ->where('nip', $id)
                            ->first();

            DB::table('employees')
            ->where('nip', $id)
            ->delete();

            DB::table('employees_login')
            ->where('employee_id', $employeeId->employee_id)
            ->delete();

            DB::commit();
            return redirect(route('auth.pengguna'))->withToastSuccess('Pengguna berhasil dihapus!');

        }
        catch (\Throwable $th)
        {
            DB::rollback();
            return redirect(route('auth.pengguna'))->withToastDanger('Pengguna gagal dihapus');
        }
    }
}
