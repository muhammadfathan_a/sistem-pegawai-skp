<?php

namespace App\Http\Controllers\Unit;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;

class UnitController extends Controller
{
    public function index()
    {
        $units = DB::table('units')->paginate(10);

        return view('pages.unit.unit', [
            'units' =>  $units
        ]);
    }

    public function edit($id)
    {
        $units = DB::table('units')->where('unit_id', strtoupper($id))->get();

        return view('pages.unit.unit-edit', [
            'units' =>  $units
        ]);
    }

    public function update(Request $request)
    {
        DB::beginTransaction();
        try {
            $units  =   DB::table('units')
                            ->where('unit_id', $request->unit_id)
                            ->update([
                                'unit_id'       =>  strtoupper(str_replace('.','',str_replace(' ', '-', $request->unit_name))),
                                'unit_name'     =>  $request->unit_name,
                                'unit_detail'   =>  $request->unit_detail,
                                'updated_at'        =>  Carbon::now()
                            ]);

            DB::commit();
            return redirect(route('auth.unit-kerja'))->withToastSuccess('Unit kerja berhasil diperbarui!');

        }
        catch (\Throwable $th)
        {
            DB::rollback();
            return redirect(route('auth.unit-kerja'))->withToastDanger('Unit kerja gagal diperbarui');
        }
    }

    public function delete($id)
    {
        DB::beginTransaction();
        try {

            $units  =   DB::table('units')
                        ->where('unit_id', strtoupper($id))
                        ->delete();

            DB::commit();
            return redirect(route('auth.unit-kerja'))->withToastSuccess('Unit kerja berhasil dihapus!');

        }
        catch (\Throwable $th)
        {
            DB::rollback();
            return redirect(route('auth.unit-kerja'))->withToastDanger('Unit kerja gagal dihapus');
        }
    }

    public function unitStore(Request $request)
    {
        DB::beginTransaction();

        try {
            DB::table('units')->insert([
                'unit_id'       =>  strtoupper(str_replace('.','',str_replace(' ', '-', $request->unit_name))),
                'unit_name'     =>  $request->unit_name,
                'unit_detail'   =>  $request->unit_detail,
            ]);

            DB::commit();

            return redirect(route('auth.unit-kerja'))->withToastSuccess('Berhasil menambahkan unit!');
        }
        catch (\Exception $e)
        {
            DB::rollback();
            return redirect(route('auth.unit-kerja'))->withToastDanger('Gagal menambahkan unit');
        }
    }
}
