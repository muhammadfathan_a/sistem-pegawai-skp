<?php

namespace App\Http\Controllers\Tentang;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TentangController extends Controller
{
    public function index()
    {
        return view('pages.tentang.tentang');
    }
}
