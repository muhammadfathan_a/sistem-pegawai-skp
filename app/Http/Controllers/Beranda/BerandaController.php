<?php

namespace App\Http\Controllers\Beranda;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use DB;

class BerandaController extends Controller
{
    public function index()
    {
        return view('pages.beranda.beranda');
    }

    public function fetchApi()
    {
        return DB::table('employees AS em')
        ->join('units AS un', 'em.unit_id', '=', 'un.unit_id')
        ->join('divisions AS dv', 'em.division_id', '=', 'dv.division_id')
        // ->where(function($query) {
        //     $query
        //     ->where('em.score', '!=', NULL)
        //     ->where('em.designation', '!=', NULL)
        //     ->where('em.information', '!=', NULL);
        //     // ->where('em.employee_id', '!=', Auth::user()->employee_id);
        // })
        ->select(
            DB::raw("CONCAT(em.title_ahead,' ', em.first_name, ' ', em.last_name, ' ', em.back_title) AS name"),
            'em.nip', 'dv.division_detail AS division_name', 'un.unit_detail AS unit_name', 'em.score',
            'em.designation', 'em.information'
        )
        ->get();
    }
}
