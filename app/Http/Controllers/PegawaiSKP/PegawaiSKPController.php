<?php

namespace App\Http\Controllers\PegawaiSKP;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use DB;

class PegawaiSKPController extends Controller
{
    public function index()
    {
        if (Auth::user()->Role->role_id == 1)
        {
            $employees  =   DB::table('employees AS em')
                            ->join('units AS un', 'em.unit_id', '=', 'un.unit_id')
                            ->join('divisions AS dv', 'em.division_id', '=', 'dv.division_id')
                            ->get();

            $units = DB::table('units')->get();
            $divisions = DB::table('divisions')->get();
        }
        else {
            $employees1  =   DB::table('employees AS em')
                            ->join('units AS un', 'em.unit_id', '=', 'un.unit_id')
                            ->join('divisions AS dv', 'em.division_id', '=', 'dv.division_id')
                            ->leftJoin('employees_login AS el', 'em.employee_id', '=', 'el.employee_id')
                            ->leftJoin('roles AS ro', 'ro.role_id', '=', 'el.role_id')
                            ->where(function($query) {
                                $query
                                ->where('un.unit_id', '=', Auth::user()->Employee->unit_id)
                                ->where('ro.role_id', '=', Auth::user()->Role->role_id);
                                // ->where('em.employee_id', '!=', Auth::user()->employee_id);
                            })
                            ->get();

            $employees2  =   DB::table('employees AS em')
                            ->join('units AS un', 'em.unit_id', '=', 'un.unit_id')
                            ->join('divisions AS dv', 'em.division_id', '=', 'dv.division_id')
                            ->leftJoin('employees_login AS el', 'em.employee_id', '=', 'el.employee_id')
                            ->leftJoin('roles AS ro', 'ro.role_id', '=', 'el.role_id')
                            ->where(function($query) {
                                $query
                                ->where('un.unit_id', '=', Auth::user()->Employee->unit_id)
                                ->whereNull('ro.role_id');
                                // ->where('em.employee_id', '!=', Auth::user()->employee_id);
                            })
                            ->get();

            $employees = $employees1->merge($employees2)->all();

            $units = DB::table('units')->where('unit_id', Auth::user()->Employee->unit_id)->get();
            $divisions = DB::table('divisions')->get();
        }

        return view('pages.pegawai-skp.pegawai-skp', [
            'employees' =>  $employees,
            'units' =>  $units,
            'divisions' =>  $divisions,
        ]);
    }

    public function fetchApi()
    {
        if (Auth::user()->Role->role_id == 1)
        {
            return DB::table('employees AS em')
            ->join('units AS un', 'em.unit_id', '=', 'un.unit_id')
            ->join('divisions AS dv', 'em.division_id', '=', 'dv.division_id')
            // ->where(function($query) {
            //     $query
            //     ->where('em.score', '!=', NULL)
            //     ->where('em.designation', '!=', NULL)
            //     ->where('em.information', '!=', NULL);
            //     // ->where('em.employee_id', '!=', Auth::user()->employee_id);
            // })
            ->select(
                DB::raw("CONCAT(em.title_ahead,' ', em.first_name, ' ', em.last_name, ' ', em.back_title) AS name"),
                'em.nip', 'dv.division_detail AS division_name', 'un.unit_detail AS unit_name', 'em.score',
                'em.designation', 'em.information'
            )
            ->get();
        }
        else {
            return DB::table('employees AS em')
            ->join('units AS un', 'em.unit_id', '=', 'un.unit_id')
            ->join('divisions AS dv', 'em.division_id', '=', 'dv.division_id')
            ->where(function($query) {
                $query
                // ->where('em.score', '!=', NULL)
                // ->where('em.designation', '!=', NULL)
                // ->where('em.information', '!=', NULL)
                ->where('un.unit_id', '=', Auth::user()->Employee->unit_id);
                // ->where('em.employee_id', '!=', Auth::user()->employee_id);
            })
            ->select(
                DB::raw("CONCAT(em.title_ahead,' ', em.first_name, ' ', em.last_name, ' ', em.back_title) AS name"),
                'em.nip', 'dv.division_detail AS division_name', 'un.unit_detail AS unit_name', 'em.score',
                'em.designation', 'em.information'
            )
            ->get();
        }
    }

    public $nip, $firstName, $lastName, $titleAhead, $backTitle, $division,
    $score, $designation, $information;
    public function employeeStore(Request $request)
    {
        // return $request;
        $this->nip = $request->nip;
        $this->firstName = $request->first_name;
        $this->lastName = $request->last_name;
        $this->titleAhead = $request->title_ahead;
        $this->backTitle = $request->back_title;
        $this->unit = $request->unit;
        $this->division = $request->division;
        $this->score = $request->score;
        $this->designation = $request->designation;
        $this->information = $request->information;

        DB::beginTransaction();
        try {
            DB::table('employees')->insert([
                'nip'           =>  $this->nip,
                'first_name'    =>  $this->firstName,
                'last_name'     =>  $this->lastName,
                'title_ahead'   =>  $this->titleAhead,
                'back_title'    =>  $this->backTitle,
                'division_id'   =>  $this->division,
                'unit_id'       =>  $this->unit,
                'score'         =>  $this->score,
                'designation'   =>  $this->designation,
                'information'   =>  $this->information,
            ]);

            DB::commit();

            return redirect(route('auth.pegawai-skp'))->withToastSuccess('Berhasil menambahkan pegawai skp!');;
        }
        catch (\Exception $e)
        {
            DB::rollback();
            return redirect(route('auth.pegawai-skp'))->withToastDanger('Gagal menambahkan pegawai skp');
        }
    }

    public function employeeChoose(Request $request)
    {
        // return $request;
        DB::beginTransaction();
        try {
            DB::table('employees')
            ->where('employee_id', $request->employee)
            ->update([
                'score'         =>  $request->score,
                'designation'   =>  $request->designation,
                'information'   =>  $request->information
            ]);

            DB::commit();
            return redirect(route('auth.pegawai-skp'))->withToastSuccess('Pegawai SKP berhasil ditambahkan!');
        }
        catch (\Throwable $th)
        {
            DB::rollback();
            return redirect(route('auth.pegawai-skp'))->withToastSuccess('Pegawai SKP berhasil ditambahkan!');
        }
    }

    public function employeeDelete(Request $request)
    {
        // return $request;
        DB::beginTransaction();
        try {
            DB::table('employees')
            ->where('employee_id', $request->employee)
            ->delete();

            DB::commit();
            return redirect(route('auth.pegawai-skp'))->withToastSuccess('Pegawai SKP berhasil dihapus!');
        }
        catch (\Throwable $th)
        {
            DB::rollback();
            return redirect(route('auth.pegawai-skp'))->withToastSuccess('Pegawai SKP berhasil dihapus!');
        }
    }
}
