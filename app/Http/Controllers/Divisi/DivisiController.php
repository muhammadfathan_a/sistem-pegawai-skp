<?php

namespace App\Http\Controllers\Divisi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Alert;
use Date;
use DB;

class DivisiController extends Controller
{
    public function index()
    {
        $divisions = DB::table('divisions')->paginate(10);

        return view('pages.divisi.divisi', [
            'divisions' =>  $divisions
        ]);
    }

    public function edit($id)
    {
        $divisions = DB::table('divisions')->where('division_id', strtoupper($id))->get();

        return view('pages.divisi.divisi-edit', [
            'divisions' =>  $divisions
        ]);
    }

    public function update(Request $request)
    {
        DB::beginTransaction();
        try {
            $divisions  =   DB::table('divisions')
                            ->where('division_id', $request->division_id)
                            ->update([
                                'division_id'       =>  strtoupper(str_replace('.','', str_replace(' ', '-', $request->division_name))),
                                'division_name'     =>  $request->division_name,
                                'division_detail'   =>  $request->division_detail,
                                'updated_at'        =>  Carbon::now()
                            ]);

            DB::commit();
            return redirect(route('auth.divisi'))->withToastSuccess('Divisi berhasil diperbarui!');

        }
        catch (\Throwable $th)
        {
            DB::rollback();
            return redirect(route('auth.divisi'))->withToastDanger('Divisi gagal diperbarui');
        }
    }

    public function delete($id)
    {
        DB::beginTransaction();
        try {

            $divisions  =   DB::table('divisions')
                            ->where('division_id', strtoupper($id))
                            ->delete();

            DB::commit();
            return redirect(route('auth.divisi'))->withToastSuccess('Divisi berhasil dihapus!');

        }
        catch (\Throwable $th)
        {
            DB::rollback();
            return redirect(route('auth.divisi'))->withToastDanger('Divisi gagal dihapus');
        }
    }

    public function divisionStore(Request $request)
    {
        DB::beginTransaction();

        try {
            DB::table('divisions')->insert([
                'division_id'       =>  strtoupper(str_replace('.','', str_replace(' ', '-', $request->division_name))),
                'division_name'     =>  $request->division_name,
                'division_detail'   =>  $request->division_detail,
            ]);

            DB::commit();

            return redirect(route('auth.divisi'))->withToastSuccess('Berhasil menambahkan divisi!');
        }
        catch (\Exception $e)
        {
            DB::rollback();
            return redirect(route('auth.divisi'))->withToastDanger('Gagal menambahkan divisi');
        }
    }
}
